package main

import (
	"fmt"
	"time"
)

type Operator interface {
	Balance() int64
	Withdraw(amount int64) error
	Deposit(amount int64) error
	Transactions() []Tx
}

type ActionKind string

const (
	ActionKindIncr ActionKind = "+"
	ActionKindDecr ActionKind = "-"
)

type Tx struct {
	value     int64      // значение на которое изменилось
	action    ActionKind // действие, прибавляем или отнимаем
	createdAt time.Time
}

// Нужно вывести данные транзакции в формате сумма: +-value, time: время создания транзакции
func (t Tx) Print() string {
	return ""
}

var _ Operator = (*txOperator)(nil)

type txOperator struct{}

func (t *txOperator) Balance() int64 {
	// TODO implement me
	panic("implement me")
}

func (t *txOperator) Withdraw(amount int64) error {
	// TODO implement me
	panic("implement me")
}

func (t *txOperator) Deposit(amount int64) error {
	// TODO implement me
	panic("implement me")
}

func (t *txOperator) Transactions() []Tx {
	// TODO implement me
	panic("implement me")
}

func main() {
	var op Operator = &txOperator{}
	_ = op
	if err := op.Withdraw(100); err != nil {
		fmt.Println("Ошибка списания", err)
	}
	fmt.Println("Текущий баланс", op.Balance())
	op.Deposit(100)
	fmt.Println("Текущий баланс", op.Balance())
	if err := op.Withdraw(100); err != nil {
		fmt.Println("Ошибка списания", err)
	}
	fmt.Println("Текущий баланс", op.Balance())
}
